import 'dart:io';
import 'dart:math';

// import 'package:midterm/midterm.dart' as midterm;

void main(List<String> arguments) {
  print("Enter mathematical expression:");
  String? mathExpression = stdin.readLineSync();
  List<String> token = create_token(mathExpression);
  print(token); //output ex.1
  List<String> postfix = infix_to_postfix(token);
  print(postfix); //output ex.2
  double ans = evaluate_postfix(postfix);
  print(ans);  //output ex.3
}

//exercise 1
create_token(var me) {
  List<String> token_list = [];
  token_list = me.split(' ');
  int tl_length = token_list.length;
  for (int i = 0; i < tl_length; i++) {
    token_list.remove('');
  }
  return token_list;
}

//exercise 2
infix_to_postfix(var token) {
  List<String> operators = [];
  List<String> postfix = [];
  int precedence, last_precedence = 10;

  for (int i = 0; i < token.length; i++) {
    if (token[i] == "0" ||
        token[i] == "1" ||
        token[i] == "2" ||
        token[i] == "3" ||
        token[i] == "4" ||
        token[i] == "5" ||
        token[i] == "6" ||
        token[i] == "7" ||
        token[i] == "8" ||
        token[i] == "9") {
      postfix.add(token[i]);
    }
    if (token[i] == "+" ||
        token[i] == "-" ||
        token[i] == "*" ||
        token[i] == "/" ||
        token[i] == "%" ||
        token[i] == "**") {
      if (token[i] == "(" || token[i] == ")") {
        precedence = 0;
      } else if (token[i] == "+" || token[i] == "-") {
        precedence = 1;
      } else if (token[i] == "*" || token[i] == "/") {
        precedence = 2;
      } else {
        precedence = 3;
      }
      if (operators.length > 0) {
        if (operators.last == "(" || operators.last == ")") {
          last_precedence = 0;
        } else if (operators.last == "+" || operators.last == "-") {
          last_precedence = 1;
        } else if (operators.last == "*" || operators.last == "/") {
          last_precedence = 2;
        } else {
          last_precedence = 3;
        }
      }
      while (operators.isNotEmpty &&
          operators.last != "(" &&
          precedence <= last_precedence) {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.add(token[i]);
    }
    if (token[i] == "(") {
      operators.add(token[i]);
    }
    if (token[i] == ")") {
      while (operators.last != "(") {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.remove("(");
    }
  }
  while (operators.isNotEmpty) {
    String keep = operators.last;
    operators.removeLast();
    postfix.add(keep);
  }
  return postfix;
}

//exercise 3
evaluate_postfix(var postfix) {
  List<double> values = [];
  double Number;
  for (int i = 0; i < postfix.length; i++) {
    if (postfix[i] == "0" ||
        postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9") {
      switch (postfix[i]) {
        case "0":
          {
            Number = 0;
            values.add(Number);
          }
          break;
        case "1":
          {
            Number = 1;
            values.add(Number);
          }
          break;
        case "2":
          {
            Number = 2;
            values.add(Number);
          }
          break;
        case "3":
          {
            Number = 3;
            values.add(Number);
          }
          break;
        case "4":
          {
            Number = 4;
            values.add(Number);
          }
          break;
        case "5":
          {
            Number = 5;
            values.add(Number);
          }
          break;
        case "6":
          {
            Number = 6;
            values.add(Number);
          }
          break;
        case "7":
          {
            Number = 7;
            values.add(Number);
          }
          break;
        case "8":
          {
            Number = 8;
            values.add(Number);
          }
          break;
        case "9":
          {
            Number = 9;
            values.add(Number);
          }
          break;
      }
    } else {
      double Right;
      double Left;
      Right = values.last;
      values.removeLast();
      Left = values.last;
      values.removeLast();
      double result = 0; 
      if (postfix[i] == "+") {
        result = (Left + Right);
      }
      if (postfix[i] == "-") {
        result = (Left - Right);
      }
      if (postfix[i] == "*") {
        result = (Left * Right);
      }
      if (postfix[i] == "/") {
        result = (Left / Right);
      }
      if (postfix[i] == "**") {
        result = (pow(Left, Right)+ 0.0);
      }
      if (postfix[i] == "%") {
        result = (Left % Right);
      }
      values.add(result);
    }
  }
  return values[0];
}
 
